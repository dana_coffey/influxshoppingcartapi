﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;
using InsiteConnector2.Models;

namespace InsiteConnector2.Controllers
{
    public class ConnectController : ApiController
    {
        WebCalls webCalls = new WebCalls();

        [HttpPost]
        [EnableCors(origins:"*", headers:"*", methods:"*")]
        public async Task<HttpResponseMessage> SubmitCart(PostData postData)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            string data = new JavaScriptSerializer().Serialize(postData);

            System.IO.File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory+"\\log.txt",data+"\r\n\r\n");

            string theContent;
            string theToken;
            bool loggedIn;

            try
            {
                loggedIn = webCalls.IsLoggedIn(postData);
            } 
            catch
            {
                loggedIn = false;
            }

            try
            {
                theToken = webCalls.GetAccessToken(postData.email);
            }
            catch
            {
                theToken = "";
            }

            if (loggedIn && !string.IsNullOrEmpty(theToken))
            {
                webCalls.RemoveHeldProducts(postData.email);

                List<InsiteProduct> productList = await webCalls.GetProducts(postData.products, theToken);
                List<Products> productsToSend = new List<Products>();
                foreach (InsiteProduct product in productList)
                {
                    Products newProd = new Products();
                    newProd.productId = product.id;
                    newProd.qtyOrdered = postData.products.Find(x => x.sku == product.erpNumber).qty;
    
                    productsToSend.Add(newProd);
                }

                if (productsToSend.Count != 0)
                {
                    theContent = await webCalls.AddToCart(productsToSend, theToken);
                }
                else
                {
                    theContent = "No products found.";
                    response.StatusCode = HttpStatusCode.ExpectationFailed;
                }
            }
            else
            {
                if (!loggedIn)
                {
                    theContent = "Not loggeed in";
                    response.StatusCode = HttpStatusCode.Unauthorized;
                }
                else
                {
                    theContent = "No access token.";
                }
            }
            
            response.Content = new StringContent(theContent, Encoding.UTF8, "text/html");
            
            return response;
        }

        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public async Task<HttpResponseMessage> SaveAccessToken(PostData postData)
        {
            await webCalls.CheckForProducts(postData);

            var responseText = webCalls.SaveAccessToken(postData.userID, postData.accessToken);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(responseText, Encoding.UTF8, "text/html");

            return response;
        }

        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public async Task<HttpResponseMessage> RemoveAccessToken(PostData postData)
        {
            webCalls.RemoveAccessToken(postData.accessToken);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent("OK", Encoding.UTF8, "text/html");

            return response;
        }
    }
}
