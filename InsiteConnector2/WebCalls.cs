﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services.Discovery;
using InsiteConnector2.Models;

namespace InsiteConnector2
{
    public class WebCalls
    {
        public async  Task<List<InsiteProduct>> GetProducts(List<InfluxProduct> influxProducts, string authToken)
        {
            string theResponse = "";
            string theERPS = "";

            for (int i = 0; i < influxProducts.Count; i++)
            {
                if (i == 0)
                {
                    theERPS += "?erpNumbers[]=" + influxProducts[i].sku;
                }
                else
                {
                    theERPS += "&erpNumbers[]=" + influxProducts[i].sku;
                }
            }

            WebRequest request = WebRequest.Create(ConfigurationManager.AppSettings["insiteURL"]+"/api/v1/products"+theERPS);
            request.Method = "GET";
            request.Headers.Add("Authorization", "Bearer " + authToken);

            try
            {
                WebResponse response = request.GetResponse();

                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);

                theResponse = reader.ReadToEnd();

                reader.Close();
                response.Close();
            }
            catch (WebException e)
            {
                WebResponse response = e.Response;

                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);

                theResponse = reader.ReadToEnd();

                reader.Close();
                response.Close();
            }

            InsiteProductList products = new InsiteProductList(theResponse);

            return products.ProductList;
        }

        public async Task<string> AddToCart(List<Products> products, string theToken)
        {
            string responseString = "";
            JavaScriptSerializer jss = new JavaScriptSerializer();
            string theProds = jss.Serialize(products);

            string data = "{\"cartLines\" : " + theProds + "}";

            HttpClient aClient = new HttpClient();

            Uri theUri = new Uri(ConfigurationManager.AppSettings["insiteURL"]+"/api/v1/carts/current/cartlines/batch");
            aClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            aClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/javascript"));
            aClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
            aClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + theToken);
            aClient.DefaultRequestHeaders.Add("Origin",HttpContext.Current.Request.Url.Host);

            StringContent theContent = new StringContent(data,Encoding.UTF8,"application/json");
            HttpResponseMessage aResponse = await aClient.PostAsync(theUri, theContent);

            if (aResponse.IsSuccessStatusCode)
            {
                StreamReader reader = new StreamReader(await aResponse.Content.ReadAsStreamAsync());
                responseString = reader.ReadToEnd();
                reader.Close();
            }
            else
            {
                responseString = aResponse.ReasonPhrase;
            }

            return responseString;
        }

        public string GetAccessToken(string email)
        {
            string connStr = ConfigurationManager.ConnectionStrings["Insite.Commerce"]
                .ConnectionString;

            using (SqlConnection con = new SqlConnection(connStr))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter("SELECT AspNetUserTokens.AccessToken FROM AspNetUserTokens LEFT JOIN AspNetUsers ON AspNetUserTokens.UserId=AspNetUsers.Id WHERE AspNetUsers.Email='"+email+"'",con);
                DataTable dt = new DataTable();
                da.Fill(dt);

                return dt.Rows[0]["AccessToken"].ToString();
            }
        }

        public bool IsLoggedIn(PostData postData)
        {
            string connStr = ConfigurationManager.ConnectionStrings["Insite.Commerce"]
                .ConnectionString;

            bool loggedIn = false;

            using (SqlConnection con = new SqlConnection(connStr))
            {
                con.Open();

                string productString = new JavaScriptSerializer().Serialize(postData.products);

                string sel = "SELECT Id FROM dbo.AspNetUsers WHERE Email = '" + postData.email + "'";
                SqlDataAdapter da1 = new SqlDataAdapter(sel, con);
                DataTable userTable = new DataTable();
                da1.Fill(userTable);

                string userId = userTable.Rows[0]["Id"].ToString();

                SqlCommand cmd = new SqlCommand("INSERT INTO ProductHolding (UserId,ProductList) VALUES ('" + userId + "','" + productString + "')",con);
                cmd.ExecuteNonQuery();

                SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM [identity].Tokens WHERE SubjectId='" + userId + "'", con);
                DataTable table = new DataTable();
                da.Fill(table);

                if (table.Rows.Count > 0)
                {
                    loggedIn = true;
                }
            }

            return loggedIn;
        }

        public void RemoveHeldProducts(string email)
        {
            string connStr = ConfigurationManager.ConnectionStrings["Insite.Commerce"]
                .ConnectionString;

            using (SqlConnection con = new SqlConnection(connStr))
            {
                con.Open();

                string sel = "SELECT Id FROM dbo.AspNetUsers WHERE Email = '" + email + "'";
                SqlDataAdapter da1 = new SqlDataAdapter(sel, con);
                DataTable userTable = new DataTable();
                da1.Fill(userTable);

                string userId = userTable.Rows[0]["Id"].ToString();

                SqlCommand cmd = new SqlCommand("DELETE FROM ProductHolding WHERE UserId='" + userId + "'", con);
                cmd.ExecuteNonQuery();

                con.Close();
            }
        }

        public async Task CheckForProducts(PostData postData)
        {
            string connStr = ConfigurationManager.ConnectionStrings["Insite.Commerce"]
                .ConnectionString;

            using (SqlConnection con = new SqlConnection(connStr))
            {
                con.Open();

                string sel = "SELECT Id FROM dbo.AspNetUsers WHERE UserName = '" + postData.userID + "'";
                SqlDataAdapter da1 = new SqlDataAdapter(sel, con);
                DataTable userTable = new DataTable();
                da1.Fill(userTable);

                string userId = userTable.Rows[0]["Id"].ToString();

                SqlDataAdapter da2 = new SqlDataAdapter("SELECT * FROM ProductHolding WHERE UserId='"+userId+"'",con);
                DataTable productTable = new DataTable();
                da2.Fill(productTable);

                if (productTable.Rows.Count > 0)
                {
                    string productString = productTable.Rows[0]["ProductList"].ToString();

                    List<InfluxProduct> products = new JavaScriptSerializer().Deserialize<List<InfluxProduct>>(productString);

                    List<InsiteProduct> productList = await GetProducts(products, postData.accessToken);

                    List<Products> productsToSend = new List<Products>();
                    foreach (InsiteProduct product in productList)
                    {
                        Products newProd = new Products();
                        newProd.productId = product.id;
                        newProd.qtyOrdered = products.Find(x => x.sku == product.erpNumber).qty;

                        productsToSend.Add(newProd);
                    }

                    if (productsToSend.Count != 0)
                    {
                        string theContent = await AddToCart(productsToSend, postData.accessToken);
                    }

                    SqlCommand cmd = new SqlCommand("DELETE FROM ProductHolding WHERE UserId='" + userId + "'", con);
                    cmd.ExecuteNonQuery();
                }

                con.Close();
            }
        }

        public void RemoveAccessToken(string accessToken)
        {
            string connStr = ConfigurationManager.ConnectionStrings["Insite.Commerce"]
                .ConnectionString;

            using (SqlConnection con = new SqlConnection(connStr))
            {
                con.Open();
                SqlCommand cmd1 = new SqlCommand("DELETE FROM AspNetUserTokens WHERE AccessToken='" + accessToken + "'",con);
                try
                {
                    cmd1.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "\\ATLog.txt", e.Message + " - " + DateTime.Now + "\r\n\r\n");
                }

                con.Close();
            }
        }

        public string SaveAccessToken(string userID, string accessToken)
        {
            string connStr = ConfigurationManager.ConnectionStrings["Insite.Commerce"]
                .ConnectionString;

            using (SqlConnection con = new SqlConnection(connStr))
            {
                con.Open();
                string sel = "SELECT * FROM AspNetUsers WHERE UserName = '" + userID + "'";
                SqlDataAdapter da1 = new SqlDataAdapter(sel,con);
                DataTable userTable = new DataTable();
                da1.Fill(userTable);

                string userId = userTable.Rows[0]["Id"].ToString();

                SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM AspNetUserTokens WHERE UserId='"+userId+"'",con);
                DataTable table = new DataTable();
                da.Fill(table);

                string command;

                if (table.Rows.Count > 0)
                {
                    command = "UPDATE AspNetUserTokens SET AccessToken='" + accessToken + "' WHERE UserId='" + userId +
                              "'";
                }
                else
                {
                    command = "INSERT INTO AspNetUserTokens (UserId,AccessToken) VALUES ('" + userId + "','" +
                              accessToken + "')";
                }

                SqlCommand cmd = new SqlCommand(command,con);
                cmd.ExecuteNonQuery();
                con.Close();
            }

            return "Done";
        }
    }
}