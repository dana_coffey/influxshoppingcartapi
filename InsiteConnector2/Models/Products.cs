﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using InsiteConnector2.Models;

namespace InsiteConnector2.Models
{
    public class InfluxProduct
    {
        public string sku { get; set; }
        public int qty { get; set; }
    }

    public class PostData
    {
        public List<InfluxProduct> products { get; set; }
        public string userID { get; set; }
        public string accessToken { get; set; }
        public string email { get; set; }
    }

    public class InsiteProduct
    {
        public string id { get; set; }
        public string erpNumber { get; set; }
    }

    public class InsiteProductList : List<InsiteProduct>
    {
        public InsiteProductList(string json)
        {
            JObject jObject = JObject.Parse(json);
            ProductList = jObject["products"].ToObject<List<InsiteProduct>>();
        }

        public List<InsiteProduct> ProductList { get; set; }
    }

    public class Products
    {
        public string productId { get; set; }
        public int qtyOrdered { get; set; }
    }
}